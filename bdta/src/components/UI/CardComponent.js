import { Card } from 'antd';
import React, { useState } from "react";

// const { Meta } = Card
import style from '../../../styles/Home.module.scss'
import Image from 'next/image'

const CardComponent = (props) => {
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };
  


  return (
  <Card
  hoverable
  style={{borderRadius: 10}}
  cover={<Image 
          layout="responsive"
          alt="example" 
          src={props.src} 
          width={450}
          height={300}
        
          />}>
        <div className={style.cardContainer} >
          <h3><span>▶ </span>{props.title}</h3>
          <div style={{marginTop: 15}}>
            <p style={{fontSize: 20, lineHeight: '1.8em', color: '#3F3F3F', textAlign:'left' }}>
              {props.content.length > props.length ? (<>
                { isReadMore ? props.content.slice(0, (props.length - 20)) + '...' : props.content } 
              <span onClick={toggleReadMore} className="read-or-hide" style={{color: 'blue'}}>
                {isReadMore ? " read more" :  " show less"}
              </span>
              </>
              ) : props.content}
            </p>
          </div>
        </div>
     
  </Card>
  )
  };
  
  export default CardComponent;