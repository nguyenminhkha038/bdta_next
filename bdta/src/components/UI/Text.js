/** @format */

import { useEffect, useRef } from 'react'
import { motion, useAnimation, useInView } from 'framer-motion'
import style from '../../../styles/Home.module.scss'

const Text = (props) => {
  const ref = useRef(null)
  const isInView = useInView(ref, { once: true })
  const controls = useAnimation()
  useEffect(() => {
    if (isInView) {
      controls.start({ x: 0, delay: 0.5 })
    }
  }, [isInView, controls])
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      viewport={{ once: true }}
      transition={{ delay: 0.5 }}
      style={props.style}
    >
      {props.children}
    </motion.div>
    // <p className={style.text}>{props.children}</p>
  )
}

export default Text