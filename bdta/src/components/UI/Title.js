import style from '../../../styles/Home.module.scss';

const Title = (props) => {
  return (
    <h3 className={style.title} style={props.style}>
      {props.children}
    </h3>
  )
}

export default Title
