import style from '../../../styles/Home.module.scss';
import Link from 'next/link'

const Button = (props) => {
  return (
    // <a href={props.href} className={style.link}>
    //     {props.children}
    // </a>
    <Link href={props.href} >
      <a style={props.style} className={style.link}>
        {props.children}
      </a>

    </Link>
  )
}

export default Button
