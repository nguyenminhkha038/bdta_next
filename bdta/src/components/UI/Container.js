import style from '../../../styles/Home.module.scss';

const container = (props) => {
  return (
    <div className={style.container} style={props.ortherStyle}>
        {props.children}
    </div>
  )
}

export default container
