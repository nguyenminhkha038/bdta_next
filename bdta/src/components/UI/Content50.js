import style from '../../../styles/Home.module.scss';


const Content50 = (props) => {
  return (
    <div className={style.content} style={props.style}>
        {props.children}
    </div>
  )
}

export default Content50
