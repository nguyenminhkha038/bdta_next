/** @format */
import style from '../../../styles/Home.module.scss'
import Form from '../form/Form'

const index = () => {
  return (
    <section className={style.loginBg}>
      <Form />
    </section>
  )
}

export default index
