import { useEffect, useState } from 'react';
import Container from '../UI/Container';
import Detail from './Detail';
import style from '../../../styles/Home.module.scss';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';

const DetailList = () => {
  const [listImage, setListImage] = useState([])

  useEffect(()=> {
    const fetchStandList = async() => {
      const res = await axios.get('https://dkgh.vj-digital.com/api/v1/listStand')
      setListImage(res.data.data)
    }
    fetchStandList()
  },[])

   
    const col = 4;
    const total = listImage.length % 4 == 0 ? listImage.length / 4 : listImage.length / 4 + 1;

    const renderList = Array.from({ length: total }, (_, i) =>
    listImage.slice(i * col, (i + 1) * col)
    );

    

  return (
    <Container ortherStyle={{ marginTop: 80, marginBottom: 50 }}>
      <h1 className={style.contentTitle} style={{ color: '#212529' }}>
          <span>CÁC GIAN HÀNG TRIỂN LÃM</span>
        <span style={{ color: '#cf2030' }}> BUSINESS MATCHING LẦN 3</span>
      </h1>
      {renderList.map((logoList) => (
        <div key={uuidv4()} className={style.partnerGrid} >
          {logoList.map((item) => (
            <div key={uuidv4()} className={`${style.partnerItem} ${style.partnerItemStore}`}>
               <Detail item={item} />
            </div>
          ))}
        </div>
      ))}

    </Container>
  );
};

export default DetailList;
