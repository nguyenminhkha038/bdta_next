import React, { useState, useRef, useEffect, useCallback } from 'react';

import Dropdown from './Dropdown';

const CustomSelect = ({ data, value, name, onChange, error, defaultOptionLabel, searchPlaceholder }) => {
  const [selectedValue, setSelectedValue] = useState(value);
  const [search, setSearch] = useState('');
  const [options, setOptions] = useState(data);
  const [showDropdown, setShowDropdown] = useState(false);
  const dropdownEl = useRef();


 
  // Hide dropdown if clicked outside of dropdown
  const handleClickOutside = useCallback((e) => {
    if(showDropdown && e.target.closest('.dropdown') !== dropdownEl.current) {
      setShowDropdown(false);
      setSearch('');
      setOptions(data);
    }
  }, [showDropdown, setShowDropdown, dropdownEl, data]);

  useEffect(() => {
    document.addEventListener('click', handleClickOutside);

    return () => {
      document.removeEventListener('click', handleClickOutside);
    }
  }, [handleClickOutside]);

  const changeSelectedHandler = (item, name) => {
    setSelectedValue(item);
    // setSelectedIndex(index);
    setShowDropdown(false);
    onChange(item, name);
  }

  const searchChangeHandler = (e) => {
    setSearch(e.target.value);
    const length = e.target.value.length;
    const filteredOptions = []
    if(e.target.value.trim().toLowerCase().slice(0,3)==='bni') {
       filteredOptions = data.filter(opt => {
        return opt.name.slice(0,length).toLowerCase().includes(e.target.value.trim().toLowerCase());
      });
    } else {
       filteredOptions = data.filter(opt => {
        return opt.name.slice(4, 4+length).toLowerCase().includes(e.target.value.trim().toLowerCase());
      });
    }

    // const filteredOptions = data.filter(opt => {
    //   return opt.name.slice(0,length).toLowerCase().includes(e.target.value.trim().toLowerCase());
    // });
    setOptions(filteredOptions);
  }

  return(
    <div className="form__group">
      <div className="dropdown" ref={dropdownEl}>
        <input type="hidden" name={name} value={value} onChange={(e) => onChange(value, name)} />
        <div className="dropdown__selected" onClick={() => setShowDropdown(!showDropdown)}>
          { selectedValue ? data.find(item => item.id === selectedValue).name : defaultOptionLabel ? defaultOptionLabel : 'Chọn chapter' }
        </div>
        {showDropdown && 
          <Dropdown 
            searchPlaceholder={searchPlaceholder}
            search={search}
            searchChangeHandler={searchChangeHandler}
            options={options}
            selectedValue={selectedValue}
            // selectedIndex={selectedIndex}
            changeSelectedHandler={changeSelectedHandler}
            name={name}
            data={data}
          />
        }
      </div>
      {/* {error && <div className="error">{error}</div>} */}
    </div>
  );
}

export default CustomSelect;