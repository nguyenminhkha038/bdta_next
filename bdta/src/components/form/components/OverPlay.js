import React from 'react'
import style from '../../../../styles/Home.module.scss'
import RingLoader from "react-spinners/RingLoader";
                    
const OverPlay = ({loading}) => {
  return (
    <div className={style.overPlay}>
      <RingLoader color='teal' loading={loading} size={100} />
    </div>
  )
}

export default OverPlay
