import React, { useRef, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import style from '../../../styles/Home.module.scss';
import { motion } from 'framer-motion';

const Statistical = ({ item }) => {
  return (
    <>
      <div className={style.statisticalContainer}>
        <div style={{ textAlign: 'center' }}>
          <h2 style={{ textAlign: 'center' }} className={style.counterService}>
            {item.counter}
          </h2>
          <h5 className={style.titleService}>{item.text}</h5>
        </div>
        <div
          style={{
            textAlign: 'center',
            margin: 'auto',
            width: '100%',
            padding: 15,
          }}
        >
          <FontAwesomeIcon
            style={{
              fontWeight: 900,
              color: '#B73232',
              width: '20%',
              margin: 'auto',
            }}
            icon={item.icon}
          />
        </div>
      </div>
    </>
  );
};

export default Statistical;
