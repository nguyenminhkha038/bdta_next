import Link from 'next/link';
import Statistical from './Statistical';
import style from '../../../styles/Home.module.scss';
import { v4 as uuidv4 } from 'uuid';

const ListStatistical = ({ list, title, src }) => {
  return (
    <div style={{ paddingLeft: 21, paddingRight: 21, marginBottom: 50 }}>
      <>
        <h1 className={style.contentTitle}>
          <span>Thống kê số liệu từ</span>{' '}
          <Link href={src}>
            <a style={{ color: '#cf2030', textDecoration: 'underline 2px' }}>
              {title}
            </a>
          </Link>
        </h1>

        <div className={style.groupData}>
          {list.map((item, index) => {
            return (
              <div
                className={style.groupDataItem}
                key={uuidv4()}
                span={8}
                xs={12}
                sm={24}
                lg={6}
              >
                <Statistical item={item} />
              </div>
            );
          })}
        </div>
      </>
    </div>
  );
};

export default ListStatistical;
