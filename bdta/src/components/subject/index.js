import style from '../../../styles/Home.module.scss';
import Wrapper from '../UI/Wrapper';
import Container from '../UI/Container';
import Link from 'next/link';
import Content3 from '../overview/Content3';

import 'antd/dist/antd.css';
import Image from 'next/image';

import number11 from '../../../public/number11.png';
import number12 from '../../../public/number12.png';



const index = (props) => {
  return (
    <div className={style.subjectContainer}>
      <Wrapper>
        <div className={style.subjectSession}>
          <h1 className={style.contentTitle}>
            {' '}
            <span style={{ fontSize: 30, color: '#cf2030' }}>ĐỐI TƯỢNG</span>{' '}
            <span style={{ fontSize: 30 }}>THAM GIA</span>
          </h1>
          <div className={style.subjectGrid}>
            <div className={style.subjectItem}>
              <Image
                // layout='responsive'
                width={900}
                height={850}
                src={number11}
              />
            </div>
            <div className={style.subjectItem}>
              <p>
                <b>CHỦ DOANH NGHIỆP TẠI TP.HCM</b>
              </p>
              <p>
                (Riêng ngành Bảo hiểm nhân thọ/Phi nhân thọ/Ngân hàng thì Giám
                đốc chi nhánh hoặc Trưởng phòng có thể tham gia)
              </p>
            </div>
          </div>
          <div className={style.subjectGrid}>
            <div className={style.subjectItem}>
              <Image
                // layout='responsive'
                width={900}
                height={850}
                src={number12}
              />
            </div>
            <div className={style.subjectItem}>
              <p>
                <b style={{ textTransform: 'uppercase' }}>
                  Chưa tham gia là thành viên của bất kỳ một chapter nào
                </b>
              </p>
            </div>
          </div>
          <div className={style.hotline}>
            <div className={style.hotlineContainer}>
              <Link href='tel://18008087'>
                <a className={style.hotlineLeft}>
                  <span
                    className={style.hotlineNumber}
                    style={{ fontWeight: 400 }}
                  >
                    Hotline: <span className={style.phone}>1800 8087</span>
                  </span>
                </a>
              </Link>
              <Link role='button' tabindex='0' href='#form' target='_self'>
                <a className={style.btnRegister}>Đăng ký tham gia</a>
              </Link>
            </div>
          </div>
        </div>

        <Container>
          <Content3 style={{ marginTop: 100, padding: 0 }} />
        </Container>

        
      </Wrapper>
    </div>
  );
};

export default index;
