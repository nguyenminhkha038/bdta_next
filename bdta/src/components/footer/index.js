import style from '../../../styles/Home.module.scss'
import Content40 from '../UI/Content40';
import Wrapper from '../UI/Wrapper';
import Title from '../UI/Title';

import Container from '../UI/Container'
import Line from '../UI/Line'
import Image from 'next/future/image'

import Link from 'next/link'
const index = () => {
    return (
        <Wrapper otherClass={style.footer}>
            <Container ortherStyle={{ display: 'flex', flexWrap: 'wrap', margin: 'auto' }}>
                <div className={style.img20}>
                    <Image 
                    // layout='responsive'
                    width={300}
                    height={400}
                    src='/logoFooter.png' />
                </div>
                <Content40>
                    <Title style={{ color: 'white', fontSize: '1.25rem', fontWeight: 600, marginBottom: 12, textAlign: 'left', padding: 0 }}>BNI - BUSINESS NETWORK INTERNATIONAL</Title>
                    <p style={{ color: 'white' }}>BNI - Business Network International là tổ chức kết nối thương mại lớn nhất và thành công nhất trên thế giới hiện nay, được Tiến sĩ Ivan Misner sáng lập vào năm 1985.</p>
                </Content40>
                <Content40>
                    <Title style={{ color: 'white', fontSize: '1.25rem', fontWeight: 600, textAlign: 'left', padding: 0 }}>CONTACT INFO</Title>
                    <div className={style.contact} >
                        <Link href='https://bnicbd.com/'>
                            <a className={style.footerLink}>Website: bnicbd.com/</a>
                        </Link>
                        <Link href='tel:1800.8087'>
                            <a className={style.footerLink}>
                                Phone : 1800.8087
                            </a>

                        </Link>
                        <Link href='mailto:hcmc6@bni.vn'>
                            <a className={style.footerLink}>
                                Email: hcmc6@bni.vn
                            </a>
                        </Link>
                        <Link href='www.facebook.com/BNI.HCM.Central6/'>
                            <a className={style.footerLink}>
                                Fanpage: www.facebook.com/BNI.HCM.Central6/
                            </a>
                        </Link>
                        <Link href='https://www.facebook.com/BNI.HCM.Central6'>
                            <a>
                                <Image width={200} height={200} layout='responsive' className={style.iframeFooter} src='/linkFB.png' />
                            </a>
                        </Link>
                    </div>
                </Content40>
                <Line style={{ marginBottom: 20 }} />
                <Title style={{ color: 'white', fontSize: 16, fontWeight: 400, margin: 'auto', paddingBottom: 30 }}>© 2022 All Rights Reserved By VIET JAPAN DIGITAL CO., LTD</Title>
            </Container>
        </Wrapper>


    )
}

export default index
