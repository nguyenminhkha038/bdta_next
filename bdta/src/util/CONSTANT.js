export const CONSTANT = {
    CONTENTCONNECT : 'Giúp chủ doanh nghiệp quảng bá thương hiệu, trao đổi giao thương và kết nối các cơ hội hợp tác kinh doanh trong cộng đồng CEO BNI đồng thời kiến tạo thương hiệu cá nhân cho Chủ doanh nghiệp - Bí quyết để sở hữu nguồn Marketing vô hạn.',
    CONTENTCOMPANION: 'Chỉ có tại các Chapter thuộc vùng BNI HCM Central 6. Chủ doanh nghiệp được huấn luyện theo Power Team bởi các nhà Huấn luyện doanh nghiệp xuất sắc nhất đến từ ActionCOACH CBD Firm, giúp phát triển nền móng vững chắc cho doanh nghiệp, nâng cao năng lực lãnh đạo, quản trị, chiến lược và vận hành doanh nghiệp hiệu quả.',
    CONTENTTREND: 'Ứng dụng thực tế theo nền kinh tế số cùng kỷ nguyên số, cập nhật thêm các kiến thức, kỹ năng và xu hướng mới nhất ở cả Việt Nam và Thế giới.',
    CONTENT_POSTER: 'Sự kiện lần này của vùng HCM Central 6 quy tụ 8 Chapter, 100 gian hàng của các thành viên sẽ đưa doanh nghiệp tiếp cận đến với hơn 800 chủ doanh nghiệp tham quan trong vòng 02 ngày, với mục tiêu "Better Together - Cùng nhau trở nên tốt hơn" giúp các Thành viên có cơ hội tương tác và kết nối giao thương với nhau, nhằm tìm kiếm các đối tác kinh doanh phù hợp và trao đi các cơ hội kinh doanh. Con số dự kiến được thống kê sau 2 đợt BUSINESS MATCHING lần 1-2 thì Lần thứ 3 sẽ có được 700 Cơ hội được trao tại chỗ và giá trị lên đến 900 Triệu '
}

