import React from 'react'
import Image from 'next/future/image';
import style from '../../styles/Home.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faGlobe, faEnvelope, faLocationDot } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';


const Detail = ({ detail }) => {
  return (
    <div className={style.detail}>
      <div className={style.detailContainer}>
        <div className={style.detailInfo}>
          <div className={style.infoLogo}>
            <Image
              src={detail.logoImage}
              width={520}
              height={250}
              alt='detail'
            />
          </div>
          <h4 className={style.infoName}>{detail.fullName}</h4>
          <ul className={style.infoList}>
            <li className={style.infoItem}>
              <FontAwesomeIcon icon={faPhone} />
              <p>{detail.phone}</p>
            </li>
            <li className={style.infoItem}>
              <FontAwesomeIcon icon={faEnvelope} />
              <p>{detail.email}</p>
            </li>
            <li className={style.infoItem}>
              <FontAwesomeIcon icon={faGlobe} />
              <Link href={detail.linkWeb}>
                <a>{detail.linkWeb}</a>

              </Link>
            </li>
            <li className={style.infoItem}>
              <FontAwesomeIcon icon={faLocationDot} />
              <p>{detail.address}</p>
            </li>
          </ul>
        </div>
        <div className={style.detailContents}>
          <p className={style.contentsStatus}>Gian hàng: đang cập nhật</p>
          <h1 className={style.contentsCompany}>{detail.companyName}</h1>
          <p className={style.contentsBusiness}>
            Ngành nghề độc quyền
            <span> {detail.career}</span>
          </p>
          <div className={style.contentsService}>
            <div dangerouslySetInnerHTML={{ __html: detail.business }}></div>
          </div>
          <hr />
          <div className={style.contentsProduct}>
            <h1 className={style.contentsProductHeading}>Sản phẩm / dịch vụ</h1>
            <div className={style.contentsProductList}>
              {detail.imgServices &&
                detail.imgServices.length > 0 &&
                detail.imgServices.map((product, index) => (
                  <div className={style.contentsProductItem} key={product[index]}>
                    <Image
                      // layout='responsive'
                      width={600}
                      height={350}
                      src={product}
                      alt=''
                    />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export async function getStaticPaths() {

  let paths = []

  try {

    let res = await fetch('https://dkgh.vj-digital.com/api/v1/listStand')

    const data = await res.json()

    paths = data.data.map((detail) => ({

      params: { id: detail.id.toString() },

    }))

    if (!res.ok) {

      throw new Error('Không thể lấy được dữ liệu');

    }

  } catch (err) {



  }



  return { paths, fallback: false }

}



export async function getStaticProps({ params }) {

  let detail;

  try {

    const res = await fetch(`https://dkgh.vj-digital.com/api/v1/stand/detail/${params.id}`)

    detail = await res.json()



    if (!res.ok) {

      throw new Error('Không thể lấy được dữ liệu');

    }

  } catch (err) {



  }

  return { props: { detail: detail.data[0] } }

}


export default Detail
